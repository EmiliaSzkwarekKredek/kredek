﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ESzkwarekLab2.Controller;
using ESzkwarekLab2.View;

namespace ESzkwarekLab2
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            dataGridViewArrivals.Rows.Add(textBoxRegisterNumber.Text, textBoxSupply.Text, textBoxAmount.Text);
        }

        private void buttonSaveToFile_Click(object sender, EventArgs e)
        {
            FormMainController.SaveToFile(dataGridViewArrivals);
        }

        private void buttonOpenFromFile_Click(object sender, EventArgs e)
        {
            FormMainController.OpenFromFile(dataGridViewArrivals);
        }

        private void textBoxChooseRegisterNumber_TextChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridViewArrivals.RowCount - 1; ++i)
            {
                if (dataGridViewArrivals[0, i].Value.ToString().Contains(textBoxChooseRegisterNumber.Text) &&
                    dataGridViewArrivals[1, i].Value.ToString().Contains(textBoxChooseSupply.Text) &&
                    dataGridViewArrivals[2, i].Value.ToString().Contains(textBoxChooseAmount.Text))
                {
                    dataGridViewArrivals.Rows[i].Visible = true;

                }
                else
                {
                    dataGridViewArrivals.Rows[i].Visible = false;
                }
            }
        }

        private void textBoxChooseSupply_TextChanged(object sender, EventArgs e)
        {
            textBoxChooseRegisterNumber_TextChanged(null, null);/*
            for (int i = 0; i < dataGridViewArrivals.RowCount - 1; ++i)
            {
                if (dataGridViewArrivals[1, i].Value.ToString().Contains(textBoxChooseSupply.Text))
                {
                    dataGridViewArrivals.Rows[i].Visible = true;
                }
                else
                {
                    dataGridViewArrivals.Rows[i].Visible = false;
                }
            }*/
        }

        private void textBoxChooseAmount_TextChanged(object sender, EventArgs e)
        {
            textBoxChooseRegisterNumber_TextChanged(null, null);
            /*
            for (int i = 0; i < dataGridViewArrivals.RowCount - 1; ++i)
            {
                if (dataGridViewArrivals[2, i].Value.ToString().Contains(textBoxChooseAmount.Text))
                {
                    dataGridViewArrivals.Rows[i].Visible = true;
                }
                else
                {
                    dataGridViewArrivals.Rows[i].Visible = false;
                }
            }*/
        }

        private void buttonResizer_Click(object sender, EventArgs e)
        {
            FormResize resizer=new FormResize(this);
            resizer.Show();

        }

        private void buttonGenerator_Click(object sender, EventArgs e)
        {
            FormGenerator generator=new FormGenerator(textBoxRegisterNumber, textBoxAmount);
            generator.Show();
        }
    }
}
