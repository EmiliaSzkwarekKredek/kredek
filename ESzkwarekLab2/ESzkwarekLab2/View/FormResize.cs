﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ESzkwarekLab2.View
{
    public partial class FormResize : Form
    {
        private FormMain windowToResize;
        private int originalWidth;
        private int originalHeight;
        public FormResize(FormMain formMain)
        {
            InitializeComponent();
            windowToResize = formMain;
            originalWidth = windowToResize.Width;
            originalHeight = windowToResize.Height;
            trackBarSize.Value = 5;
        }

        private void trackBarSize_ValueChanged(object sender, EventArgs e)
        {
            windowToResize.Width = trackBarSize.Value*originalWidth/5;
            windowToResize.Height = trackBarSize.Value*originalHeight/5;
        }
    }
}
