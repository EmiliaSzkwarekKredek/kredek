﻿namespace ESzkwarekLab2
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNew = new System.Windows.Forms.Button();
            this.labelRegisterNumber = new System.Windows.Forms.Label();
            this.labelSupply = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.textBoxRegisterNumber = new System.Windows.Forms.TextBox();
            this.textBoxSupply = new System.Windows.Forms.TextBox();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.dataGridViewArrivals = new System.Windows.Forms.DataGridView();
            this.ColumnRegisterNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSupply = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSaveToFile = new System.Windows.Forms.Button();
            this.buttonOpenFromFile = new System.Windows.Forms.Button();
            this.textBoxChooseRegisterNumber = new System.Windows.Forms.TextBox();
            this.textBoxChooseSupply = new System.Windows.Forms.TextBox();
            this.textBoxChooseAmount = new System.Windows.Forms.TextBox();
            this.labelChooseRegisterNumber = new System.Windows.Forms.Label();
            this.labelChooseSupply = new System.Windows.Forms.Label();
            this.labelChooseAmount = new System.Windows.Forms.Label();
            this.buttonResizer = new System.Windows.Forms.Button();
            this.buttonGenerator = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArrivals)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(23, 22);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(123, 23);
            this.buttonNew.TabIndex = 0;
            this.buttonNew.Text = "Nowy przyjazd";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // labelRegisterNumber
            // 
            this.labelRegisterNumber.AutoSize = true;
            this.labelRegisterNumber.Location = new System.Drawing.Point(184, 9);
            this.labelRegisterNumber.Name = "labelRegisterNumber";
            this.labelRegisterNumber.Size = new System.Drawing.Size(99, 13);
            this.labelRegisterNumber.TabIndex = 1;
            this.labelRegisterNumber.Text = "Numer rejestracyjny";
            // 
            // labelSupply
            // 
            this.labelSupply.AutoSize = true;
            this.labelSupply.Location = new System.Drawing.Point(325, 9);
            this.labelSupply.Name = "labelSupply";
            this.labelSupply.Size = new System.Drawing.Size(37, 13);
            this.labelSupply.TabIndex = 2;
            this.labelSupply.Text = "Towar";
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = true;
            this.labelAmount.Location = new System.Drawing.Point(466, 9);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(29, 13);
            this.labelAmount.TabIndex = 3;
            this.labelAmount.Text = "Ilość";
            // 
            // textBoxRegisterNumber
            // 
            this.textBoxRegisterNumber.Location = new System.Drawing.Point(187, 24);
            this.textBoxRegisterNumber.Name = "textBoxRegisterNumber";
            this.textBoxRegisterNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegisterNumber.TabIndex = 4;
            this.textBoxRegisterNumber.Text = "WYT2800";
            // 
            // textBoxSupply
            // 
            this.textBoxSupply.Location = new System.Drawing.Point(328, 24);
            this.textBoxSupply.Name = "textBoxSupply";
            this.textBoxSupply.Size = new System.Drawing.Size(100, 20);
            this.textBoxSupply.TabIndex = 5;
            this.textBoxSupply.Text = "komputer";
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(469, 26);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(100, 20);
            this.textBoxAmount.TabIndex = 6;
            this.textBoxAmount.Text = "10";
            // 
            // dataGridViewArrivals
            // 
            this.dataGridViewArrivals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewArrivals.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewArrivals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArrivals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnRegisterNumber,
            this.ColumnSupply,
            this.ColumnAmount});
            this.dataGridViewArrivals.Location = new System.Drawing.Point(23, 52);
            this.dataGridViewArrivals.Name = "dataGridViewArrivals";
            this.dataGridViewArrivals.Size = new System.Drawing.Size(546, 181);
            this.dataGridViewArrivals.TabIndex = 7;
            // 
            // ColumnRegisterNumber
            // 
            this.ColumnRegisterNumber.HeaderText = "Numer rejestracyjny";
            this.ColumnRegisterNumber.Name = "ColumnRegisterNumber";
            // 
            // ColumnSupply
            // 
            this.ColumnSupply.HeaderText = "Towar";
            this.ColumnSupply.Name = "ColumnSupply";
            // 
            // ColumnAmount
            // 
            this.ColumnAmount.HeaderText = "Ilość";
            this.ColumnAmount.Name = "ColumnAmount";
            // 
            // buttonSaveToFile
            // 
            this.buttonSaveToFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSaveToFile.Location = new System.Drawing.Point(23, 242);
            this.buttonSaveToFile.Name = "buttonSaveToFile";
            this.buttonSaveToFile.Size = new System.Drawing.Size(123, 23);
            this.buttonSaveToFile.TabIndex = 8;
            this.buttonSaveToFile.Text = "Zapisz do pliku";
            this.buttonSaveToFile.UseVisualStyleBackColor = true;
            this.buttonSaveToFile.Click += new System.EventHandler(this.buttonSaveToFile_Click);
            // 
            // buttonOpenFromFile
            // 
            this.buttonOpenFromFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOpenFromFile.Location = new System.Drawing.Point(160, 242);
            this.buttonOpenFromFile.Name = "buttonOpenFromFile";
            this.buttonOpenFromFile.Size = new System.Drawing.Size(123, 23);
            this.buttonOpenFromFile.TabIndex = 9;
            this.buttonOpenFromFile.Text = "Otwórz z pliku";
            this.buttonOpenFromFile.UseVisualStyleBackColor = true;
            this.buttonOpenFromFile.Click += new System.EventHandler(this.buttonOpenFromFile_Click);
            // 
            // textBoxChooseRegisterNumber
            // 
            this.textBoxChooseRegisterNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxChooseRegisterNumber.Location = new System.Drawing.Point(23, 299);
            this.textBoxChooseRegisterNumber.Name = "textBoxChooseRegisterNumber";
            this.textBoxChooseRegisterNumber.Size = new System.Drawing.Size(130, 20);
            this.textBoxChooseRegisterNumber.TabIndex = 10;
            this.textBoxChooseRegisterNumber.TextChanged += new System.EventHandler(this.textBoxChooseRegisterNumber_TextChanged);
            // 
            // textBoxChooseSupply
            // 
            this.textBoxChooseSupply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxChooseSupply.Location = new System.Drawing.Point(231, 299);
            this.textBoxChooseSupply.Name = "textBoxChooseSupply";
            this.textBoxChooseSupply.Size = new System.Drawing.Size(130, 20);
            this.textBoxChooseSupply.TabIndex = 11;
            this.textBoxChooseSupply.TextChanged += new System.EventHandler(this.textBoxChooseSupply_TextChanged);
            // 
            // textBoxChooseAmount
            // 
            this.textBoxChooseAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxChooseAmount.Location = new System.Drawing.Point(439, 299);
            this.textBoxChooseAmount.Name = "textBoxChooseAmount";
            this.textBoxChooseAmount.Size = new System.Drawing.Size(130, 20);
            this.textBoxChooseAmount.TabIndex = 12;
            this.textBoxChooseAmount.TextChanged += new System.EventHandler(this.textBoxChooseAmount_TextChanged);
            // 
            // labelChooseRegisterNumber
            // 
            this.labelChooseRegisterNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelChooseRegisterNumber.AutoSize = true;
            this.labelChooseRegisterNumber.Location = new System.Drawing.Point(20, 274);
            this.labelChooseRegisterNumber.Name = "labelChooseRegisterNumber";
            this.labelChooseRegisterNumber.Size = new System.Drawing.Size(130, 13);
            this.labelChooseRegisterNumber.TabIndex = 13;
            this.labelChooseRegisterNumber.Text = "Podaj numer rejestracyjny:";
            // 
            // labelChooseSupply
            // 
            this.labelChooseSupply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelChooseSupply.AutoSize = true;
            this.labelChooseSupply.Location = new System.Drawing.Point(228, 274);
            this.labelChooseSupply.Name = "labelChooseSupply";
            this.labelChooseSupply.Size = new System.Drawing.Size(103, 13);
            this.labelChooseSupply.TabIndex = 14;
            this.labelChooseSupply.Text = "Podaj rodzaj towaru:";
            // 
            // labelChooseAmount
            // 
            this.labelChooseAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelChooseAmount.AutoSize = true;
            this.labelChooseAmount.Location = new System.Drawing.Point(436, 274);
            this.labelChooseAmount.Name = "labelChooseAmount";
            this.labelChooseAmount.Size = new System.Drawing.Size(61, 13);
            this.labelChooseAmount.TabIndex = 15;
            this.labelChooseAmount.Text = "Podaj ilość:";
            // 
            // buttonResizer
            // 
            this.buttonResizer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonResizer.Location = new System.Drawing.Point(314, 242);
            this.buttonResizer.Name = "buttonResizer";
            this.buttonResizer.Size = new System.Drawing.Size(123, 23);
            this.buttonResizer.TabIndex = 16;
            this.buttonResizer.Text = "Skalowanie";
            this.buttonResizer.UseVisualStyleBackColor = true;
            this.buttonResizer.Click += new System.EventHandler(this.buttonResizer_Click);
            // 
            // buttonGenerator
            // 
            this.buttonGenerator.Location = new System.Drawing.Point(455, 242);
            this.buttonGenerator.Name = "buttonGenerator";
            this.buttonGenerator.Size = new System.Drawing.Size(114, 23);
            this.buttonGenerator.TabIndex = 17;
            this.buttonGenerator.Text = "Generowanie";
            this.buttonGenerator.UseVisualStyleBackColor = true;
            this.buttonGenerator.Click += new System.EventHandler(this.buttonGenerator_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 349);
            this.Controls.Add(this.buttonGenerator);
            this.Controls.Add(this.buttonResizer);
            this.Controls.Add(this.labelChooseAmount);
            this.Controls.Add(this.labelChooseSupply);
            this.Controls.Add(this.labelChooseRegisterNumber);
            this.Controls.Add(this.textBoxChooseAmount);
            this.Controls.Add(this.textBoxChooseSupply);
            this.Controls.Add(this.textBoxChooseRegisterNumber);
            this.Controls.Add(this.buttonOpenFromFile);
            this.Controls.Add(this.buttonSaveToFile);
            this.Controls.Add(this.dataGridViewArrivals);
            this.Controls.Add(this.textBoxAmount);
            this.Controls.Add(this.textBoxSupply);
            this.Controls.Add(this.textBoxRegisterNumber);
            this.Controls.Add(this.labelAmount);
            this.Controls.Add(this.labelSupply);
            this.Controls.Add(this.labelRegisterNumber);
            this.Controls.Add(this.buttonNew);
            this.Name = "FormMain";
            this.Text = "Rejestr pojazdów";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArrivals)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Label labelRegisterNumber;
        private System.Windows.Forms.Label labelSupply;
        private System.Windows.Forms.Label labelAmount;
        private System.Windows.Forms.TextBox textBoxRegisterNumber;
        private System.Windows.Forms.TextBox textBoxSupply;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.DataGridView dataGridViewArrivals;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRegisterNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSupply;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAmount;
        private System.Windows.Forms.Button buttonSaveToFile;
        private System.Windows.Forms.Button buttonOpenFromFile;
        private System.Windows.Forms.TextBox textBoxChooseRegisterNumber;
        private System.Windows.Forms.TextBox textBoxChooseSupply;
        private System.Windows.Forms.TextBox textBoxChooseAmount;
        private System.Windows.Forms.Label labelChooseRegisterNumber;
        private System.Windows.Forms.Label labelChooseSupply;
        private System.Windows.Forms.Label labelChooseAmount;
        private System.Windows.Forms.Button buttonResizer;
        private System.Windows.Forms.Button buttonGenerator;
    }
}

