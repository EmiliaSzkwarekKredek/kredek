﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ESzkwarekLab2.View
{
    public partial class FormGenerator : Form
    {
        private TextBox registerNumber;
        private TextBox amount;

        public FormGenerator(TextBox textBoxRegisterNumber, TextBox textBoxAmount)
        {
            InitializeComponent();
            registerNumber = textBoxRegisterNumber;
            amount = textBoxAmount;
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            var random = new Random();
            registerNumber.Text = "DW" + random.Next(1000, 9999);
            amount.Text = random.Next(1, 100).ToString();
        }
    }
}
