﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ESzkwarekLab2.Controller
{
    public static class FormMainController
    {
        public static void SaveToFile(DataGridView dataGridViewArrivals)
        {
            StreamWriter stream = new StreamWriter("test.txt");
            for (int i = 0; i < dataGridViewArrivals.RowCount - 1; i++)
            {
                for (int j = 0; j < dataGridViewArrivals.ColumnCount; j++)
                {
                    stream.Write(dataGridViewArrivals.Rows[i].Cells[j].Value + ";");
                }
                stream.Write(stream.NewLine);
            }
            stream.Close();
        }

        public static void OpenFromFile(DataGridView dataGridViewArrivals)
        {
            StreamReader stream = new StreamReader("test.txt");
            while (!stream.EndOfStream)
            {
                string line = stream.ReadLine();
                var elements = line.Split(';').ToList();
                dataGridViewArrivals.Rows.Add(elements.GetRange(0, 3).ToArray());
            }
            stream.Close();
        }
    }
}
